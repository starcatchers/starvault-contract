import { expect } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { BigNumber } from "@ethersproject/bignumber";
const { time } = require('@openzeppelin/test-helpers');

import { StarVault, StarVault__factory } from "../typechain-types";

describe("StarVault Contract", async ()=> {

    let sv: StarVault;
	let owner: SignerWithAddress
    let payees: Array<SignerWithAddress>
	let vestDays: number
	let cliffDays: number
	let failsafeDays: number

	beforeEach(async ()=> {
		payees = (await ethers.getSigners()).slice(0, 4);
		owner = payees[0];
		cliffDays = 365;
		vestDays = 1460;
		failsafeDays = 7;
		const svFactory = (await ethers.getContractFactory(
			"StarVault", owner
		)) as StarVault__factory;
		sv = await svFactory.deploy(payees.map(a => a.address), cliffDays, vestDays, failsafeDays);
	});

	describe("Deployment", ()=> {
		it("Initializes with appropriate payees", async ()=> {
            for (let i = 0; i < payees.length; i++) {
				let addressExists = await sv.vaultPayees(payees[i].address);
                expect(addressExists).to.equal(true);
            }
		});
	});

	describe("Timer", ()=> {
		it("Can start timer only once", async ()=> {
			await sv.startTimer()
            await expect(sv.startTimer())
				.to.be.revertedWith('Timer already started');
		});

		it("Vest timers are accurate", async ()=> {
			await sv.startTimer()
			let now = (await ethers.provider.getBlock(await ethers.provider.getBlockNumber())).timestamp
            expect(await sv.startTimestamp()).to.equal(now);
		});

		it("Cannot claim before start", async ()=> {
            await expect(sv.claim(owner.address))
				.to.be.revertedWith('Vest timestamp not set');
		});
	});

	describe("Claim", ()=> {
		let now: number
		let initiateValue: BigNumber

		beforeEach(async ()=> {
			// Send contract 1k eth.
			initiateValue = ethers.utils.parseEther("1000.0");
			await owner.sendTransaction({
				to: sv.address,
				value: initiateValue,
			});
			expect(await ethers.provider.getBalance(sv.address)).to.equal(initiateValue);

			await sv.startTimer();
			let claimable = await sv.maxClaimable(owner.address);
            expect(claimable).to.equal(0);
			now = (await ethers.provider.getBlock(await ethers.provider.getBlockNumber())).timestamp;
		});

		it("Claim amount one day into schedule is 0", async ()=> {
			let future = now + 86400;
			await ethers.provider.send("evm_mine", [future]);
			let claimable = await sv.maxClaimable(owner.address);
            expect(claimable).to.equal(0);
		});

		it("Claim amount right after cliff is appropriate", async ()=> {
			let future = now + Number(time.duration.days(cliffDays));
			await ethers.provider.send("evm_mine", [future]);
			let claimable = await sv.maxClaimable(owner.address);
            expect(claimable).to.equal(initiateValue.div(payees.length).div(time.duration.days(vestDays) / time.duration.days(cliffDays)));
		});

		it("Payee ledger prevents over claim", async ()=> {
			let future = now + Number(time.duration.days(cliffDays));
			await ethers.provider.send("evm_mine", [future]);
			await sv.claim(owner.address);
			expect(await sv.maxClaimable(owner.address)).to.equal(0);
		});

		it("Can't claim for others", async ()=> {
			let future = now + Number(time.duration.days(cliffDays));
			await ethers.provider.send("evm_mine", [future]);
			await expect(sv.maxClaimable(payees[1].address))
				.to.be.revertedWith('Claim must be for self');
		});

		it("Must be called by valid payee", async ()=> {
			let rando = (await ethers.getSigners())[payees.length+1]
			await expect(sv.connect(rando).claim(rando.address))
				.to.be.revertedWith('Invalid payee');
		});

		it("Daily claim sim", async ()=> {
			// Zoom to end of cliff
			let future = now + Number(time.duration.days(cliffDays));
			await ethers.provider.send("evm_mine", [future]);

			// Grab cliff accrued
			let total = initiateValue.div(payees.length);
			let totalReceived = (await sv.maxClaimable(owner.address));
			expect(totalReceived).to.equal(total.div(vestDays / cliffDays));
			await sv.claim(owner.address);

			// Claim correct amount daily
			// note: allow small rounding error as sol / contract rounds down math for safety.
			let allowedRoundingError = ethers.utils.parseEther("0.00001");
			for (let i = 1; i <= vestDays - cliffDays; i++) {
				await ethers.provider.send("evm_mine", [future+Number(time.duration.days(i))]);
				let got = await sv.maxClaimable(owner.address);
				let want = total.div(vestDays);
				expect(want.sub(got).lte(allowedRoundingError)).to.equal(true);
				await sv.claim(owner.address);
				totalReceived = totalReceived.add(got);
			}

			expect(await sv.maxClaimable(owner.address)).to.equal(0);
			allowedRoundingError = allowedRoundingError.mul(vestDays - cliffDays);
			expect(total.sub(totalReceived).lte(allowedRoundingError)).to.equal(true);
		}).timeout(20000);
	});

	describe("ClaimAll", ()=> {
		let now: number
		let initiateValue: BigNumber

		beforeEach(async ()=> {
			// Send contract 1k eth.
			initiateValue = ethers.utils.parseEther("1000.0");
			await owner.sendTransaction({
				to: sv.address,
				value: initiateValue,
			});
			expect(await ethers.provider.getBalance(sv.address)).to.equal(initiateValue);

			await sv.startTimer();
			let claimable = await sv.maxClaimable(owner.address);
            expect(claimable).to.equal(0);
			now = (await ethers.provider.getBlock(await ethers.provider.getBlockNumber())).timestamp;
		});

		it("Fails until failsafe days after vest completes", async ()=> {
			let vestCompleteTimestamp = now + Number(time.duration.days(vestDays));
			let claimAllTimestamp = vestCompleteTimestamp + Number(time.duration.days(failsafeDays));
			await ethers.provider.send("evm_mine", [vestCompleteTimestamp]);
            await expect(sv.claimAll(owner.address))
				.to.be.revertedWith('It is too early to claimAll');
			await ethers.provider.send("evm_mine", [claimAllTimestamp]);
			await (sv.claimAll(owner.address));
		});

		it("Must be called by valid payee", async ()=> {
			let rando = (await ethers.getSigners())[payees.length+1]
			await expect(sv.connect(rando).claimAll(rando.address))
				.to.be.revertedWith('Invalid payee');
		});

		it("Can't claim 0", async ()=> {
			let claimAllTimestamp = now + Number(time.duration.days(vestDays)) + Number(time.duration.days(failsafeDays));
			await ethers.provider.send("evm_mine", [claimAllTimestamp]);
			await sv.claimAll(owner.address);
			await expect(sv.claimAll(owner.address))
				.to.be.revertedWith('Contract balance is 0');
		});

	});

});
