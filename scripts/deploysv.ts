import hre, { ethers } from "hardhat";

const deploy = async () => {
  let cliffDays = 365;
  let vestDays = 1460;
  let failsafeDays = 7;
  let vaultPayees = [
    "0x18898ac235fD3ABABF40402c8516cb32d517532d",
    "0xB0B60594c97D73A16233B112f7fFa6aB470B1790",
    "0x73abF793F58c86052d7AA11570b8F54BE0957B6c",
    "0x6e0D10C284A55Ee10a28cD7Ad9314C5B3FCA92A5"
  ];
  console.log("StarVault deploying...");
  const StarVault = await ethers.getContractFactory("StarVault");
  const sv = await StarVault.deploy(vaultPayees, cliffDays, vestDays, failsafeDays);
  await sv.deployed();
  await sv.deployTransaction.wait(5);
  await hre.run("verify:verify", {
    address: sv.address,
    constructorArguments: [
      vaultPayees,
      cliffDays,
      vestDays,
      failsafeDays
    ],
  });
  console.log("StarVault deployed to:", sv.address);
}

deploy()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
