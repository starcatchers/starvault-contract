# StarVault Contract

This is the source repository for the StarVault contract, created and used by
Starcatchers.

It implements a Pull Payments pattern for withdrawing funds based on a pre-set
vesting schedule.  Similar to OZ escrow contract, with added vesting schedule.
Payees are only settable once, inside construction.

Payees should not be contract addresses, these are explicitly disallowed in
claim functions for added safety.

Vesting begins once {startTimer} is called, {startTimer} is only callable once.
After vestDays + failSafeDays {claimAll} is unlocked.

## Usage

If you'd like to use this contract or build off this repository check
`hardhat.config.ts` for relevant environment variables to set.  Inside of
`package.json` there are scripts for frequently used tasks.  Eg.. compiling,
testing, deploying.
